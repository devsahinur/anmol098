<h2>नमस्ते (Namaste)🙏🏻, I'm Anmol Pratap Singh! <img src="https://media.giphy.com/media/12oufCB0MyZ1Go/giphy.gif" width="50"></h2>
<img align='right' src="https://media.giphy.com/media/M9gbBd9nbDrOTu1Mqx/giphy.gif" width="230">
<p><em>Software Engineer at <a href="http://www.cleartax.in">ClearTax</a><img src="https://media.giphy.com/media/WUlplcMpOCEmTGBtBW/giphy.gif" width="30"> 
</em></p>

[![Twitter Follow](https://img.shields.io/twitter/follow/misteranmol?label=Follow)](https://twitter.com/intent/follow?screen_name=misteranmol)
[![Linkedin: anmol](https://img.shields.io/badge/-anmol-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/anmol-p-singh/)](https://www.linkedin.com/in/anmol-p-singh/)
![GitHub followers](https://img.shields.io/github/followers/anmol098?label=Follow&style=social)
[![website](https://img.shields.io/badge/Website-46a2f1.svg?&style=flat-square&logo=Google-Chrome&logoColor=white&link=https://anmolsingh.me/)](https://anmolsingh.me/)
![](https://visitor-badge.glitch.me/badge?page_id=anmol098.anmol098)
![Waka Readme](https://github.com/anmol098/anmol098/workflows/Waka%20Readme/badge.svg)

👇 Hit in your console or terminal to connect with me.

```bash
npx anmol
```
**👆 This command line tool can be found at [npx anmol](https://github.com/anmol098/npx_card)**

### <img src="https://media.giphy.com/media/VgCDAzcKvsR6OM0uWg/giphy.gif" width="50"> A little more about me...  

```javascript
const anmol = {
    pronouns: "He" | "Him",
    code: ["Javascript", "Python", "Java", "Swift", "PHP"],
    askMeAbout: ["web dev", "tech", "app dev", "photography"],
    technologies: {
        backEnd: {
            js: ["Node", "Fastify", "Express", "SuiteScript"],
        },
        mobileApp: {
            native: ["Android Development", "IOS Development"]
        },
        devOps: ["AWS", "Docker🐳", "Route53", "Nginx"],
        databases: ["mongo", "MySql", "sqlite"],
        misc: ["Firebase", "Socket.IO", "selenium", "open-cv", "php", "SuiteApp"]
    },
    architecture: ["Serverless Architecture", "Progressive web applications", "Single page applications"],
    currentFocus: "Ios Mobile App Development",
    funFact: "There are two ways to write error-free programs; only the third one works"
};
```

<img src="https://media.giphy.com/media/LnQjpWaON8nhr21vNW/giphy.gif" width="60"> <em><b>I love connecting with different people</b> so if you want to say <b>hi, I'll be happy to meet you more!</b> 😊</em>

---
<!--START_SECTION:waka-->
![Profile Views](http://img.shields.io/badge/Profile%20Views-837-blue)

![Lines of code](https://img.shields.io/badge/From%20Hello%20World%20I%27ve%20Written-1.5%20million%20lines%20of%20code-blue)

**🐱 My Github Data** 

> 🏆 167 Contributions in the Year 2021
 > 
> 📦 403.5 kB Used in Github's Storage 
 > 
> 💼 Opted to Hire
 > 
> 📜 53 Public Repositories 
 > 
> 🔑 24 Private Repositories  
 > 
**I'm an Early 🐤** 

```text
🌞 Morning    56 commits     ██░░░░░░░░░░░░░░░░░░░░░░░   11.22% 
🌆 Daytime    207 commits    ██████████░░░░░░░░░░░░░░░   41.48% 
🌃 Evening    156 commits    ███████░░░░░░░░░░░░░░░░░░   31.26% 
🌙 Night      80 commits     ████░░░░░░░░░░░░░░░░░░░░░   16.03%

```
📅 **I'm Most Productive on Sunday** 

```text
Monday       51 commits     ██░░░░░░░░░░░░░░░░░░░░░░░   10.22% 
Tuesday      49 commits     ██░░░░░░░░░░░░░░░░░░░░░░░   9.82% 
Wednesday    66 commits     ███░░░░░░░░░░░░░░░░░░░░░░   13.23% 
Thursday     79 commits     ████░░░░░░░░░░░░░░░░░░░░░   15.83% 
Friday       65 commits     ███░░░░░░░░░░░░░░░░░░░░░░   13.03% 
Saturday     67 commits     ███░░░░░░░░░░░░░░░░░░░░░░   13.43% 
Sunday       122 commits    ██████░░░░░░░░░░░░░░░░░░░   24.45%

```


📊 **This Week I Spent My Time On** 

```text
⌚︎ Time Zone: Asia/Kolkata

💬 Programming Languages: 
Swift                    6 hrs 23 mins       ████████░░░░░░░░░░░░░░░░░   35.31% 
Python                   5 hrs 30 mins       ███████░░░░░░░░░░░░░░░░░░   30.4% 
C++                      2 hrs 55 mins       ████░░░░░░░░░░░░░░░░░░░░░   16.21% 
JSON                     1 hr 19 mins        █░░░░░░░░░░░░░░░░░░░░░░░░   7.3% 
SQL                      56 mins             █░░░░░░░░░░░░░░░░░░░░░░░░   5.17%

🔥 Editors: 
PyCharm                  7 hrs 5 mins        █████████░░░░░░░░░░░░░░░░   39.15% 
Xcode                    6 hrs 23 mins       ████████░░░░░░░░░░░░░░░░░   35.31% 
CLion                    2 hrs 56 mins       ████░░░░░░░░░░░░░░░░░░░░░   16.21% 
WebStorm                 50 mins             █░░░░░░░░░░░░░░░░░░░░░░░░   4.7% 
DataGrip                 50 mins             █░░░░░░░░░░░░░░░░░░░░░░░░   4.64%

💻 Operating System: 
Mac                      18 hrs 5 mins       █████████████████████████   100.0%

```

**I Mostly Code in Swift** 

```text
Swift                    13 repos            █████░░░░░░░░░░░░░░░░░░░░   20.97% 
Vue                      10 repos            ████░░░░░░░░░░░░░░░░░░░░░   16.13% 
Java                     8 repos             ███░░░░░░░░░░░░░░░░░░░░░░   12.9% 
JavaScript               8 repos             ███░░░░░░░░░░░░░░░░░░░░░░   12.9% 
C++                      7 repos             ██░░░░░░░░░░░░░░░░░░░░░░░   11.29%

```


**Timeline**

![Chart not found](https://raw.githubusercontent.com/anmol098/anmol098/master/charts/bar_graph.png) 


 Last Updated on 10/07/2021
<!--END_SECTION:waka-->

**These Readme stats are generated using github action [awesome-readme-stats](https://github.com/anmol098/waka-readme-stats)**
